// Extra JS added to all pages

// Hide code line numbers from screen readers
$("span.ln").each(function (index, value) {
	value.setAttribute('aria-hidden', true)
})

$.getScript("https://hypothes.is/embed.js");


var sc_project = 11679202;
var sc_invisible = 1;
var sc_security = "3859aea9";
$.getScript("https://www.statcounter.com/counter/counter.js")

$('img.chart').each(function () {
	var $img = $(this);
	var imgID = $img.attr('id');
	var imgClass = $img.attr('class');
	var imgURL = $img.attr('src');

	$.get(imgURL, function (data) {
		var $svg = $(data).find('svg');
		if (typeof imgID !== 'undefined') {
			$svg = $svg.attr('id', imgID);
		}
		if (typeof imgClass !== 'undefined') {
			$svg = $svg.attr('class', imgClass + ' replaced-svg');
		}
		$svg = $svg.removeAttr('xmlns:a');
		$img.replaceWith($svg);
	});
});
