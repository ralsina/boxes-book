# Issues

Ideally, in some cases, our code will have users, and since we are not perfect because what's the fun in that, those users will do things with and to our code that we don't anticipate.

In some cases, they will do things we did anticipate but find out that the outcome is not as expected. Those are bugs.

In other cases, they will want to use our code to do things our code just doesn't do. Those are feature requests.

Both are issues. And your code **will** have issues. In this chapter we will work about how you learn about those issues, how you react to them, and how they affect your life and the life of a project.

# Workflows

If you have a workflow to handle issues it gives you a clear map, tells you how you get from one place to another. Depending on context it may be enforced by software, as usually happens in corporate environments using Jira, or just be a rough preference on how things should be, as in most projects with few contributors.

I personally much prefer the latter, but *it is just a personal preference*, and yours is as good as anyone's.

Here's what I use in my open source projects:

* Have an issue tracker
* If possible, add relevant questions in the issue reporting step. For example:

  * What version of this software are you using?
  * What version of python?
  * What operating system?
  * What were you doing?
  * What happened?
  * What were you expecting to happen?
* Let anyone file things in the tracker
* Be notified about new issues or changes in open issues
* Respond promptly, even if the response is "thanks for reporting"
* Ask any relevant questions that come to mind
* Don't overpromise fixes (I fail at this **a lot**)
* Think real hard if this is a real problem and not just a user error
* Think real hard if you have the bandwidth to work on it
* Work on it
* Propose a fix in a pull request
* If it's not a one-person project, ask for reviews
* Ask the issue reporter whether it actually fixes the issue
* Merge to master
* Release, because if you don't release it's not fixed

That is **a lot.**
