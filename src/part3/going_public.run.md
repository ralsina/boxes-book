# Part 3: Going Public

Welcome to part 3 of this book!

Until now we have worked on code. We started with no code, we wrote some, we
polished it, we organized it, we tested it, we improved it and we fixed it.
Now it's time for not-code.

If you are writing code for yourself, then this part may not be very useful
for you. However, why write code just for yourself? Sharing it with others is
easy and can be very rewarding.

In the following chapters we will learn about tools that will help you become
a better developer and help you make your work easier to access for others.

Let's get to it.