# Git

In this chapter we will cover git. Git is a version control system. That means
you can use it to keep track of your code and how it changes over time. If you
keep your code "under git" you can go back to any point in time and know what
changed, why and how.

I would much rather not have to explain git. I would prefer to explain any of
the alternatives... except there aren't any. Once upon a time there were, but
they lost, and what we have is git, which is powerful, but complicated and
confusing for many.

So, I will **try** to give a very gentle introduction to a subset of git which
makes sense for amateur not-quite-beginning programmers. Just keep in mind
that there is much *more* git than shown here.

In this chapter we will focus on using git for controlling change in a local
copy of the code. In the next we will see how it can be used to coordinate
changes with a remote version, which is one of the more powerful and useful
features in git.

If you already know git, please skip this. It's only going to piss you off
because of the shortcuts and intentional inaccuracies.

## Getting Started

First you will need to install it. Go to http://www.git-scm.com/downloads and
install it like any other software.

We will be using git from the command line. There are other ways:

* It can integrate in your IDE
* You can use a GUI
* You can use alternative command line interfaces

But because I don't want to force you to use a specific tool, we will use the
most basic and ubiquitous interface for it: the `git` command.

If you already installed it, let's check if it's working:

```bash
$ git

usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

These are common Git commands used in various situations:

start a working area (see also: git help tutorial)
   clone      Clone a repository into a new directory
   init       Create an empty Git repository or reinitialize an existing one

[...]

'git help -a' and 'git help -g' list available subcommands and some
concept guides. See 'git help <command>' or 'git help <concept>'
to read about a specific subcommand or concept.
```

Let's put this out there: that help is absolutely **useless**. Don't try to
understand that usage description. That way lies madness. The only way to make
sense of it is to already be deeply familiar with git, and that would mean you
don't need it.

If you get an error instead of that help text, it could be your git is
not properly installed.

## Concepts

Here are some of the concepts you just need to remember when using git. They
can't be guessed, you just have to remember what they are.

### Repositories

A git repository is just a folder. The contents of that folder are the files
git "controls". The only "special" thing about a folder that makes it a git
repository is that it has a hidden `.git` subfolder.

The `.git` subfolder is used by git to store bookkeeping information:

* What files are important and needs to be controlled by git?
* How have those files changed in the past?
* What were those changes?

And lots more. You never need to look or modify the contents of the `.git` folder yourself except via the `git` command, but please don't delete it.

Having said that, let's create our very own repository!

```bash
$ mkdir my-repo
$ cd my-repo
$ git init
Initialized empty Git repository in my-repo/.git/
```

I created a folder, moved there and called `git init` which initialized the repo.
It says it's empty because, yes, there is nothing there.

> **Hint**
>
> Git by default operates on the current folder. Always keep in mind where
> you are standing in your filesystem when using git.

You will usually have a separate git repository (or "repo") for each project
you start.

### Changes

Git is all about keeping track of changes to your files. Those changes are of different kinds:

* Adding files
* Editing files
* Removing files

Let's go over each one.

#### Adding a file

An empty repository is not very useful, so let's make it not-so-empty by
creating a file, and try another git "subcommand", `git status`:

```bash
$ echo 'Hello git' > hello.txt
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        hello.txt

nothing added to commit but untracked files present (use "git add" to track)
```

Let's look at that output little by little.

* `On branch master` Please ignore that for a little bit. We will get to
  branches later.
* `No commits yet` Again, we will see commits very soon.
* `Untracked files: [...] hello.txt` This is the part we care about right now.

We added a file, and it appears as "untracked". Just because you put a file in the repository folder, it doesn't mean git cares about it. For that you need to "add" it.

```bash
$ git add hello.txt
$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

        new file:   hello.txt
```

The new file `hello.txt` is now in what is called the `staging` step. Every time you
do something with git, it is done in two steps:

* Stage the change
* Commit the change

We told git "hey, keep track of this file". Now we have to tell it "Make it
so". Make git **commit** to keeping track of it.

```bash
$ git commit hello.txt -m 'Added hello file'
[master (root-commit) 3e3d854] Added hello file
 1 file changed, 1 insertion(+)
 create mode 100644 hello.txt
```

Again, we need to look at this carefully.

`git commit hello.txt`

That means "git, commit the file hello.txt". Remember that we had staged a
change? Well, this commits the change. That means it's now stored in git's
memory. It now knows about that file we created and added and will never
forget about it.

If you have changed more than one file, you can either list them all in the
command, or use the `-a` option, which means "commit all files that are in the
staging state".

`-m 'Added hello file'`

When you commit something in git, you are required to give a "commit message".
It should be a brief description of what you are doing / why you are doing it.

You can either use the `-m` option like this, or just put nothing there, and
you will get a text editor where you can enter a longer description. If you do
the long description, try to do a short one in the first line, then leave a
blank and add the longer explanation there, like this:

```bash
Added hello file

I am trying to explain git, so I am starting with
adding a file

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# On branch master
#
# Initial commit
#
# Changes to be committed:
#       new file:   hello.txt
```

All the lines starting with `#` at the bottom are for your information, they
will not be part of the commit message.

> **Hint**
>
> Commit messages are **important**. If you ever use 'fixes' or 'tweaks' or
> 'minor changes' then people who are collaborating with you will have absolutely
> no idea what you are doing. Commit messages are the diary of your work. They
> should be expressive and well written.

`[master (root-commit) 3e3d854] Added hello file`

Master is the branch (we will get to that), `3e3d854` is a way to identify the
commit we just did. Each commit has one. It's actually much longer. In this
case it's `3e3d85457297b25b759e7033fab9a9075c823022` but that is a
mouthful, so there is also the "short" version which is just the beginning of it.

> **Hint**
>
> The proper name for that is actually a *revision*. Each commit creates a revision.
> In practice you can use either name most of the time.

You will use those commit identifiers in the future. For example, you may say
"oh, yes, I remember hello.txt was added in commit `3e3d854`!".

`Added hello file` is either the commit message (if you used `-m`) or the
first line of it if you used the editor.

```
1 file changed, 1 insertion(+)
create mode 100644 hello.txt
```

Git will give you some idea of what changed in this commit. Look at it to make sure it describes what you were trying to do.

```bash
$ git status
On branch master
nothing to commit, working tree clean
```

This status is what it says: there are no untracked files, no staged changes,
all good.

#### Editing a file

Lets edit `hello.txt` and change its content. I added a second line that says
"Bye git".

```bash
$ cat hello.txt
Hello git
Bye git
```

If git is any good about tracking changes to my files, it should notice.

```bash
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   hello.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

And it did notice. I will ask you to ignore git's suggestions about using `git add`
or `git commit -a`. Yes, you can do that, but there is no need, really.

Not only does git know we changed `hello.txt`, it can tell you **how**:

```bash
$ git diff
```

```diff
diff --git a/hello.txt b/hello.txt
index 0dec223..d514db9 100644
--- a/hello.txt
+++ b/hello.txt
@@ -1 +1,2 @@
 Hello git
+Bye git
```

I am not going to explain diff syntax, but here are the highlights:

* The `a/hello.txt b/hello.txt` tells you the change is in `hello.txt`
* The change is that we **added** a line, that is why it says `+Bye git`. When
  removing lines that would be a `-`

The change we made is not committed yet. We can commit it just the same as before:

```bash
$ git commit hello.txt -m 'Say bye'
[master 6ea17af] Say bye
 1 file changed, 1 insertion(+)
```

#### Deleting a file

The final change type is deleting a file.

```bash
$ rm hello.txt
$ git status
On branch master
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        deleted:    hello.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

Again, it's deleted but it's not really gone because we have not commited the
deletion.

```bash
$ git commit hello.txt -m 'Not needed anymore'
[master 606186a] Not needed anymore
 1 file changed, 2 deletions(-)
 delete mode 100644 hello.txt
```

And just like that, our file is gone. But not forever.
### History

The whole point of git, as mentioned, is to keep track of our files and how
they change. We have created, modified, and deleted a file. How can we see what
happened?

```bash
$ git log
commit 606186a457d92fd9921fbeade3b20e6b014c7120
Author: Roberto Alsina <ralsina@netmanagers.com.ar>
Date:   Sat Apr 14 13:11:22 2018 -0300

    Not needed anymore

commit 6ea17af0a2b79388a4df1f9b9540cb5caacaf5ab
Author: Roberto Alsina <ralsina@netmanagers.com.ar>
Date:   Sat Apr 14 11:06:08 2018 -0300

    Say bye

commit 3e3d85457297b25b759e7033fab9a9075c823022
Author: Roberto Alsina <ralsina@netmanagers.com.ar>
Date:   Sat Apr 14 10:31:38 2018 -0300

    Added hello file

    I am trying to explain git, so I am starting with
    adding a file
```

Git has kept a record of all the changes. And not just a record that they happened. It has the *history*. We can actually move back and forth in time through it.
Right now `hello.txt doesn't exist:

```bash
$ ls hello.txt
ls: cannot access 'hello.txt': No such file or directory
```

But in the log I see I created it in commit `3e3d etc`. So I can use
`git checkout` to bring the repo to the state in that commit:

```bash
$ git checkout 3e3d85457297b25b759e7033fab9a9075c823022
Note: checking out '3e3d85457297b25b759e7033fab9a9075c823022'.
```

Now git will give us one of its usual technically true but not really what you
need to know right now texts.

```bash
You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by performing another checkout.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -b with the checkout command again. Example:

  git checkout -b <new-branch-name>
```

And look, we are now in the "Added hello file" commit!

```bash
HEAD is now at 3e3d854 Added hello file
```

And the file exists again!

```bash
$ cat hello.txt
Hello git
```

We **could** edit it and change it and do things with it. But we shouldn't.

This is what our git history looked like after we did our last commit. The
yellow commit is what we have "checked out", which is usually the latest. In
git slang that is called `HEAD` (as in the head in a cassette tape, not head
as in a snake, that is why it's not always on the last commit).

```graphviz:git_1.svg
digraph {
    rankdir = LR
    A[label="Added hello file"]
    B[label="Say bye"]
    C[label="Not needed anymore" class="HEAD"]
    A -> B
    B -> C
}
```

After we checked out an earlier revision, this is how it looked like:

```graphviz:git_2.svg
digraph {
    rankdir = LR
    A[label="Added hello file"  class="HEAD"]
    B[label="Say bye"]
    C[label="Not needed anymore"]
    A -> B
    B -> C
}
```

So, the file exists, because we have checked out a revision when the file
existed.

But if we were to change things now, things would get weird.

```bash
$ echo 'Hello, fellow git' > hello.txt
$ git commit hello.txt -m 'Git is a fellow'
[detached HEAD 6e1e76f] Git is a fellow
 1 file changed, 1 insertion(+), 1 deletion(-)
```

See that `detached HEAD` there? Detaching heads is never good.

```bash
$ git status
HEAD detached from 3e3d854
nothing to commit, working tree clean
```

Remember how it used to say `master`? And now it says `HEAD detached`.
Let's look at the log, too.

```bash
$ git log
commit 6e1e76f98fe1c1bb1d46a395480b71503254b3c0 (HEAD)
Author: Roberto Alsina <ralsina@netmanagers.com.ar>
Date:   Sat Apr 14 13:38:27 2018 -0300

    Git is a fellow

commit 3e3d85457297b25b759e7033fab9a9075c823022
Author: Roberto Alsina <ralsina@netmanagers.com.ar>
Date:   Sat Apr 14 10:31:38 2018 -0300

    Added hello file

    I am trying to explain git, so I am starting with
    adding a file
```

What happened to the other commits? To "Say bye" and "Not needed anymore"?
They don't exist anymore. We went back in time and married their mom, so they
don't exist.

Just kidding, they exist, but this is how our history looks now:

```graphviz:git_3.svg
digraph {
    rankdir = LR
    A[label="Added hello file"]
    B[label="Say bye"]
    C[label="Not needed anymore"]
    D[label="Git is a fellow" class="HEAD"]
    subgraph cluster_master {
        label = "master"
        A -> B
        B -> C
    }
    subgraph cluster_detached {
        label = "detached HEAD"
        A -> D
    }
}
```

Any further changes we do would go in this "detached HEAD" alternative
universe and now you have basically two copies of everything and it all got
very complicated. Please don't get yourself in detached HEAD situations
unless you know what you are doing.

What you want, if you want to keep things reasonable, are branches, which we
will see shortly.

However, there is a perfectly reasonable thing to do that involves going back in
time.

You committed changes by mistake.

What happens if you just want to undo a commit? There are two ways to do it.

You committed something before it was ready. So you want to keep the changes
but remove the commit?

In this case, use `git reset --soft commit-you-want-to-keep`

```bash
$ git reset --soft 6ea17af0a2b79388a4df1f9b9540cb5caacaf5ab
$ git log
commit 6ea17af0a2b79388a4df1f9b9540cb5caacaf5ab (HEAD -> master)
Author: Roberto Alsina <ralsina@netmanagers.com.ar>
Date:   Sat Apr 14 11:06:08 2018 -0300

    Say bye

commit 3e3d85457297b25b759e7033fab9a9075c823022
Author: Roberto Alsina <ralsina@netmanagers.com.ar>
Date:   Sat Apr 14 10:31:38 2018 -0300

    Added hello file

    I am trying to explain git, so I am starting with
    adding a file
```

As you can see, the 3rd commit is gone. It's really gone! Git knows nothing
about it anymore. However, because it's a "soft" reset, the change itself
still happened:

```bash
$ ls hello.txt
ls: cannot access 'hello.txt': No such file or directory
```

You committed something and it's crap. So you want to remove all memory of the
changes. **Do not do this unless you realy have to**. For example, if you
commit your password in the repo? Yes, you want to remove all trace. But you
just made a mistake? Fix it and commit the fix. Everyone makes mistakes!

So, if you really want to lose data, with great caution use `git reset --hard
commit-you-want-to-keep`

```bash
$ git reset --hard 6ea17af0a2b79388a4df1f9b9540cb5caacaf5ab
HEAD is now at 6ea17af Say bye
```

Not only is that last commit gone now (log looks like after a `git reset
--soft`) but the change itself is gone:

```bash
$ cat hello.txt
Hello git
Bye git
```

Using `git reset` deletes the newer part of history. Now our history looks
like this:

```graphviz:git_4.svg
digraph {
    rankdir = LR
    A[label="Added hello file"]
    B[label="Say bye"  class="HEAD"]
    A -> B
}
```

> **Hint**
>
> The only difference between `--soft` and `--hard` is that changes are
> irrecoverably lost using `--hard`. It's one of the easiest ways to lose a lot
> of work. Please be careful.

### Branches

Suppose you want to work on a large change to a project. It will take several
days to finish, and you want to keep on using the code for other things in the
meantime.

Without using git, what you would do is probably make a copy, work on the
copy, use the original, and later on bring those changes back into the
original.

Suppose in the meantime, while you are working on the feature, you also need
to fix a small bug. Now, bringing the changes back is starting to look
complicated, right?

Git is meant to help handle those cases through the use of **branches**.

Here is an example. Let's start by creating an empty repo, like before.

```bash
$ mkdir my-repo
$ cd my-repo/
$ git init .

Initialized empty Git repository in /tmp/my-repo/.git/
```

Let's put a file there and commit a few revisions.

```bash
$ touch shoplist.txt
$ git add shoplist.txt
$ git commit shoplist.txt -m 'Empty shopping list'
[master (root-commit) 07dfb41] Empty shopping list
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 shoplist.txt
$ echo '* Coffee' > shoplist.txt
$ git commit shoplist.txt -m 'Buy Coffee'
[master 12a3d5e] Buy Coffee
 1 file changed, 1 insertion(+)
$ echo '* Donut' >> shoplist.txt
$ git commit shoplist.txt -m 'Buy donut'
[master 38f48fd] Buy donut
 1 file changed, 1 insertion(+)
$ echo '* Milk' >> shoplist.txt
$ git commit shoplist.txt -m 'Buy milk'
[master b3c2b3e] Buy milk
 1 file changed, 1 insertion(+)
$ cat shoplist.txt
* Coffee
* Donut
* Milk
```

Our history looks like this:

```graphviz:git_5.svg
digraph {
    rankdir = LR
    1[label="Empty shopping list"]
    2[label="Buy Coffee"]
    3[label="Buy donut"]
    4[label="Buy milk" class="HEAD"]
    1->2->3->4
}
```

Now, you want to add the list of supplies for your workshop, but you want the
list to be as-is until you are done.

We branch.

```bash
$ git checkout -b workshop
Switched to a new branch 'workshop'
$ echo "* Bandsaw" >> shoplist.txt
$ git commit shoplist.txt -m 'Buy a bandsaw for the workshop'
[workshop fda2474] Buy a bandsaw for the workshop
 1 file changed, 1 insertion(+)
$ echo "* Anvil" >> shoplist.txt
$ git commit shoplist.txt -m 'Buy anvil for the workshop'
[workshop 1eaefa3] Buy anvil for the workshop
 1 file changed, 1 insertion(+)
```

If we look at the history at this point, this is what it looks like:

```graphviz:git_6.svg
digraph {
    rankdir = LR
    1[label="Empty shopping list"]
    2[label="Buy Coffee"]
    3[label="Buy donut"]
    4[label="Buy milk"]
    5[label="Buy bandsaw"]
    6[label="Buy anvil" class="HEAD"]
    1->2->3->4->5->6
}
```

Except that is not really true. This is how it **really** looks.


```graphviz:git_7.svg
digraph {
    rankdir = LR
    subgraph cluster_master {
        label = "master"
        1[label="Empty shopping list"]
        2[label="Buy Coffee"]
        3[label="Buy donut"]
        4[label="Buy milk" class="HEAD"]
    }
    subgraph cluster_workshop {
        label = "workshop"
        5[label="Buy bandsaw"]
        6[label="Buy anvil" class="HEAD"]
    }
    1->2->3->4->5->6
}
```

Remember "master"? Master is a branch. It's the first branch in every git
repo, it always exists, and it's the default branch. If you never do `git
checkout -b` then it will always be the only branch.

When we did `git checkout -b workshop` we created a new branch, called
workshop. From that point forward, every commit belongs to that branch. You
can switch between branches using `git checkout branchname`, just like we used
to travel in history using `git checkout commit_identifier`.

To know on what branch you currently stand, you can use `git branch`

```bash
$ git branch
  master
* workshop
$ git checkout master
Switched to branch 'master'
$ git branch
* master
  workshop
```

Each branch is sort of a parallel universe:

```bash
$ git branch
* master
  workshop
$ cat shoplist.txt
* Coffee
* Donut
* Milk
$ git checkout workshop
Switched to branch 'workshop'
$ cat shoplist.txt
* Coffee
* Donut
* Milk
* Bandsaw
* Anvil
```

When you fork a branch, everything that is in the past from that branching
point is in both branches, and everything that happens later is only on the
branch where you did it.

To show this, we can go to the master branch and do something:

```bash
$ git checkout master
Switched to branch 'master'
$ echo "* Sugar" >> shoplist.txt
$ git commit shoplist.txt -m 'sugar too'
[master 47592f1] sugar too
 1 file changed, 1 insertion(+)
```

That change happened on master, but did not happen on the workshop branch:

```bash
$ git checkout workshop
Switched to branch 'workshop'
$ cat shoplist.txt
* Coffee
* Donut
* Milk
* Bandsaw
* Anvil
```

And now our history is a bit more complex:

```graphviz:git_8.svg
digraph {
    rankdir = LR
    subgraph cluster_master {
        label = "master"
        1[label="Empty shopping list"]
        2[label="Buy Coffee"]
        3[label="Buy donut"]
        4[label="Buy milk"]
        5[label="Buy sugar" class="HEAD"]
    }
    subgraph cluster_workshop {
        label = "workshop"
        w5[label="Buy bandsaw"]
        w6[label="Buy anvil" class="HEAD"]
    }
    1->2->3->4->5
    4->w5->w6
}
```

Notice how in the graph we have two HEADs? Each branch has one.

So, we can have as many branches as we want... but in the end, we probably
want to bring the changes we did in the workshop branch back into master.

That is done via `merge` and `rebase`

> **Hint**
>
> Tags mark a moment in a branch. You can:
>
> * Create a tag: `git tag tagname`
> * List the tags: `git tag -l`
> * Remove a tag: `git tag -d tagname`
> * Put the repo in the exact state marked by a tag: `git checkout tagname`
>
> Use it as a bookmark for important things. For example, if you are making a
> release of a program you are writing, tag it.
>
> I will not go into details about tags. There are lots of things about them
> online.


### Merge

Merging works like this. If you are standing in branch A and want the changes
from branch B, you say `git merge B`

So, for example, if I wanted to bring the changes in `workshop` into `master`
it would go like this:

```bash
$ git checkout master
Switched to branch 'master'
$ git merge workshop
Auto-merging shoplist.txt
CONFLICT (content): Merge conflict in shoplist.txt
Automatic merge failed; fix conflicts and then commit the result.
```

Oh, crap. Often, nothing bad will happen. Not in this case. When two branches
edit the same parts of a file, it will end up as a **conflict**. They look like this:

```bash
|||4,6,9
* Coffee
* Donut
* Milk
<<<<<<< HEAD
* Sugar
=======
* Bandsaw
* Anvil
>>>>>>> workshop
```

You have:

```diff
<<<<<<< HEAD
```

That `<<<<<<` marks the beginning of a conflict. It's followed by changes you
have in your current branch. In this case, the `* Sugar` line.

Then you have `======` which marks the end of the changes in the current
branch, and the beginning of the changes in the branch you are trying to
merge.

In the workshop branch, we had two new lines: Bandsaw and Anvil. Then there is
a `>>>>>>` marking the end of the conflict.

In some cases, depending on how complicated the merge is, how much have things
changed between branches, and how close to each other the changes are, you
will have many conflicts in one or more files.

What we have to do is **resolve** the conflict. Take that whole chunk delimited between `<<<<<<` and `>>>>>>` and fix it.

In this case, the fix is simple: we want all the items in the list!

```bash
* Coffee
* Donut
* Milk
* Sugar
* Bandsaw
* Anvil
```

Once you have fixed all the conflicts in a file, you need to tell git that you
did:

```bash
$ git add shoplist.txt
$ git status
On branch master
All conflicts fixed but you are still merging.
  (use "git commit" to conclude merge)

Changes to be committed:

        modified:   shoplist.txt
```

And now we **finish** the merge by committing (it always has to be done using
`git commit -a`):

```bash
$ git commit -a -m 'merged the workshop branch'
[master aea0f88] merged the workshop branch
```

And now our history looks like this:

```graphviz:git_9.svg
digraph {
    rankdir = LR
    subgraph cluster_master {
        label = "master"
        1[label="Empty shopping list"]
        2[label="Buy Coffee"]
        3[label="Buy donut"]
        4[label="Buy milk"]
        5[label="Buy sugar"]
        6[label="Merged workshop" class="HEAD"]
    }
    subgraph cluster_workshop {
        label = "workshop"
        w5[label="Buy bandsaw"]
        w6[label="Buy anvil" class="HEAD"]
    }
    1->2->3->4->5->6
    4->w5->w6->6
}
```

Just because we merged it, it doesn't mean anything happened to our workshop
branch. It's still there, we can still add commits to it, we could even branch
off it by doing `git checkout -b` while it's our current branch, and so on.

In fact, if you have long lived branches you often have to merge things
**back** from master into those branches so they don't diverge too much.

Often that is done via rebasing.

### Rebase

Let's go back to the status quo pre-merge. We are in this state:

<img src="git_8.svg" class="chart"/>

The difference between `git merge` and `git rebase` is this:

* Merge takes the difference between this branch and the "other" branch and
  tries to patch the current branch to have all those changes. It applies the
  changes **on top** of our last commit.

* Rebase takes the "other branch" as it is now, and tries to reapply the
  changes you made in the "current branch" on top of that. It applies the
  changes **below** our branch.

Let's try to rebase workshop off current master:

```bash
$ git checkout workshop
Switched to branch 'workshop'
$ git rebase master
First, rewinding head to replay your work on top of it...
```

It goes to our branching point from master (before the bandsaw), applies all
the changes that happened on master (buy milk) and then tries to apply each
commit from workshop.

Of course adding "bandsaw" causes a conflict:

```bash
Applying: Buy a bandsaw for the workshop
Using index info to reconstruct a base tree...
M       shoplist.txt
Falling back to patching base and 3-way merge...
Auto-merging shoplist.txt
CONFLICT (content): Merge conflict in shoplist.txt
error: Failed to merge in the changes.
Patch failed at 0001 Buy a bandsaw for the workshop
The copy of the patch that failed is found in: .git/rebase-apply/patch

When you have resolved this problem, run "git rebase --continue".
If you prefer to skip this patch, run "git rebase --skip" instead.
To check out the original branch and stop rebasing, run "git rebase --abort".
```

How does the conflict look?

```bash
|||4,6,8
* Coffee
* Donut
* Milk
<<<<<<< 47592f16f4c712b7caa444f6d3ed76e58f903eb9
* Sugar
=======
* Bandsaw
>>>>>>> Buy a bandsaw for the workshop
```

We know how to fix it:

```text
* Coffee
* Donut
* Milk
* Sugar
* Bandsaw
```

We mark the conflict as resolved using `git add` and continue the rebase:

```bash
$ git add shoplist.txt
$ git rebase --continue
Applying: Buy a bandsaw for the workshop
Applying: Buy anvil for the workshop
Using index info to reconstruct a base tree...
M       shoplist.txt
Falling back to patching base and 3-way merge...
Auto-merging shoplist.txt
```

If we look at the log, now our workshop branch **has** the changes that were
on master before its own!

```bash
$ git shortlog
Roberto Alsina (7):
      Empty shopping list
      Buy Coffee
      Buy donut
      Buy milk
      sugar too
      Buy a bandsaw for the workshop
      Buy anvil for the workshop
```

Because that is what rebase does. It takes the changes in the **other** branch, and then applies the changes in your current branch **on top** of those.

And now our history looks like this:

```graphviz:git_10.svg
digraph {
    rankdir = LR
    subgraph cluster_master {
        label = "master"
        1[label="Empty shopping list"]
        2[label="Buy Coffee"]
        3[label="Buy donut"]
        4[label="Buy milk"]
        5[label="Buy sugar" class="HEAD"]
    }
    subgraph cluster_workshop {
        label = "workshop"
        w5[label="Buy bandsaw"]
        w6[label="Buy anvil" class="HEAD"]
    }
    1->2->3->4->5->w5->w6
}
```

We moved the branching point of the workshop branch *forward* to the HEAD of
master.

The advantage of using `git rebase` to bring changes from master to our branch
is that merges from our branch *to* master will always be clean, because our
changes apply **after** those in master:

```bash
$ git checkout master
Switched to branch 'master'
$ git merge workshop
Updating 47592f1..21501f8
Fast-forward
 shoplist.txt | 2 ++
 1 file changed, 2 insertions(+)
$ git status
On branch master
nothing to commit, working directory clean
```

And our history is straightforward because now both branches are
exactly the same! There are no changes that are in master and not in
workshop, or viceversa.

```bash
$ git shortlog
Roberto Alsina (7):
      Empty shopping list
      Buy Coffee
      Buy donut
      Buy milk
      sugar too
      Buy a bandsaw for the workshop
      Buy anvil for the workshop
```

```graphviz:git_11.svg
digraph {
    rankdir = LR
    subgraph cluster_master {
        label = "master"
        1[label="Empty shopping list"]
        2[label="Buy Coffee"]
        3[label="Buy donut"]
        4[label="Buy milk"]
        5[label="Buy sugar"]
        6[label="Buy bandsaw"]
        7[label="Buy anvil" class="HEAD"]
    }
    1->2->3->4->5->6->7
}
```

Yes, the workshop branch still exists. But it looks exactly like master, so no
point in even drawing it. In fact, after a temporary branch is merged, you may
just want to delete it using `git branch -d workshop`

Why did I say **temporary** branch? Because sometimes you want to have
permanent branches. For example, some people keep a `develop` branch alive at
all times where they work, and when they want to freeze it to release a
version, they would fork a branch out for the release.

There is much, much more to know about branches and merging and other things,
but this should be enough... to work on your own. But git is at its best when
used to collaborate with others, and that is going to happen in the next
chapter.

