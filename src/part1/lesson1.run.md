# BOXES v0.1

Welcome to Boxes v0.1. I want to be able to draw some boxes. By boxes I don't mean actual boxes,
but rather squares. I found a library called svgwrite that lets you do that pretty easily.

First let's create a data structure. A simple class called Box.

<div class='source_title'><a href="code/lesson1.py.html" target="_blank">lesson1.py</a></div>

```python
&&&1
class Box():

    def __init__(self, x=0, y=0, w=1, h=1):
        """Accept arguments to define our box, and store them."""
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def __repr__(self):
        return "Box(%s, %s, %s, %s)" % (self.x, self.y, self.w, self.h)

```

As you can see that is a pretty simple class. And we can create a big box.

```python
big_box = Box(0, 0, 80, 100)
```

Or many boxes using a [list comprehension](https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions)

<div class='source_title'><a href="code/lesson1.py.html" target="_blank">lesson1.py</a></div>

```python
&&&14
many_boxes = [Box() for i in range(5000)]

```

So now we have a big box, and 5000 smaller boxes, all alike.

```python
# Print the first 10 boxes
print(many_boxes[:10])
```

```
[Box(0, 0, 1, 1), Box(0, 0, 1, 1), Box(0, 0, 1, 1), Box(0, 0, 1, 1), Box(0, 0, 1, 1), Box(0, 0, 1, 1), Box(0, 0, 1, 1), Box(0, 0, 1, 1), Box(0, 0, 1, 1), Box(0, 0, 1, 1)]
```

And yes, we can draw those boxes. Because this is python 3.6 or higher I am
using [f-strings](https://cito.github.io/blog/f-strings/). All the `cm` you
see there are because we are creating SVG images and they have *physical*
sizes, so I am doing everything in centimeters (yay metric system), to avoid
using things like the default unit, which is points (1/72 of an inch) or some
other similar nonsense.

<div class='source_title'><a href="code/lesson1.py.html" target="_blank">lesson1.py</a></div>

```python
&&&16
import svgwrite


def draw_boxes(boxes, fname, size):
    dwg = svgwrite.Drawing(fname, profile="full", size=size)
    # Draw all the boxes
    for box in boxes:
        dwg.add(
            dwg.rect(
                insert=(f"{box.x}cm", f"{box.y}cm"),
                size=(f"{box.w}cm", f"{box.h}cm"),
                fill="red",
            )
        )
    dwg.save()


draw_boxes(many_boxes, "lesson1.svg", ("5cm", "2cm"))

```
And here is the output:

![lesson1.svg](lesson1.svg)

That ... was not very interesting. It's a single small red square! Where are
all the remaining 4999 boxes?

Remember, *all our boxes have the same size and position!*

So ... we should do something better. Or at least more interesting, in lesson 2.

----------

Further references:

* Full source code for this lesson: <a href="lesson1.py" target="_blank">lesson1.py</a>

