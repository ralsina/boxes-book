# SETUP

This is a book (often) about code, so you need to setup your coding
environment. Since you already know how to code a little, I assume you know
how to use a text editor, change a file and run commands, and have Python 3.6
or later installed.

## Get the code

For this part of the book, we have a single folder with all the example code.

It's available in GitLab: [part1 example code](https://gitlab.com/ralsina/boxes-book/tree/master/src/part1/code)
or just download it in [a zip file](sources.zip)

## Setup a virtual environment

Use your favourite tool, setup a virtualenv and activate it. If you have no
idea what I mean, do this:

```bash
$ cd code
$ python -m venv venv
$ source venv/bin/activate
(venv)
$
```

After you run the `source` command, your prompt will say "venv". That means
you are working in an isolated environment and when you install / uninstall
python packages you are not breaking your system.

To go back to the real world, you can use the `deactivate` command.

## Install dependencies


```bash
$ pip install -r requirements.txt
```

## Check it worked

```bash
$ python lesson0.py
Looks good!
```

If it gives an error instead, there is a problem and we have to fix it.

You are now ready to move on to the real work. Have fun!

