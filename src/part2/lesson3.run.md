# Boxes v0.14

In the [previous lesson](lesson2.md) we mentioned *code smells*, the existence
of hardcoded "magic numbers" and global variables in our code, and we cleaned
it up. There is one other code smell that makes our code pretty funky.

The `layout` function is **long**. It's 101 lines long. That is nuts.
Functions over 50 lines are usually too complicated. It's also one of the
undesirable side effects of the process by which we wrote it. When you write
your code organically from the core of an idea, it tends to produce long
functions, because it's easier to just tweak things in place instead of
breaking things into separate functions.

So, in the future you may decide to do that along the way, or just do what we
will now do and fix it afterwards.

This functions does all the following things:

* Breaks our list of boxes into rows
* Insert hyphens if needed
* Justifies lines if needed
* Breaks the rows into pages
* Deletes leftover pages

That is a lot of responsibility for a single function. So, we need to break it
up. The most complicated part of doing this, and the part you will get better
at overtime, is figuring out where the "natural" places to break are.

The easy one is justifying rows. It's here:

<div class='source_title'><a href="code/lesson2/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&110
            # We adjust the row
            # Remove all right-margin spaces
            while row[-1].letter == " ":
                row.pop()
            slack = (pages[page].x + pages[page].w) - (
                row[-1].x + row[-1].w
            )
            # Get a list of all the ones that are stretchy
            stretchies = [b for b in row if b.stretchy]
            if not stretchies:  # Nothing stretches do as before.
                bump = slack / len(row)
                # The 1st box gets 0 bumps, the 2nd gets 1 and so on
                for i, b in enumerate(row):
                    b.x += bump * i
            else:
                bump = slack / len(stretchies)
                # Each stretchy gets wider
                for b in stretchies:
                    b.w += bump
                # And we put each thing next to the previous one
                for j, b in enumerate(row[1:], 1):
                    b.x = row[j - 1].x + row[j - 1].w + separation

```

The only inputs to that code are `row`, `pages[page]` and `separation` so
extracting it and making it a function is easy:

<div class='source_title'><a href="code/lesson3/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&59
def justify_row(row, page, separation):
    """Given a row and a page, adjust position of elements in the row
    so it fits the page width.

    It modifies the contents of the row in place, so returns nothing.
    """

    # Remove all right-margin spaces
    while row[-1].letter == " ":
        row.pop()
    slack = (page.x + page.w) - (row[-1].x + row[-1].w)
    # Get a list of all the ones that are stretchy
    stretchies = [b for b in row if b.stretchy]
    if not stretchies:  # Nothing stretches do as before.
        bump = slack / len(row)
        # The 1st box gets 0 bumps, the 2nd gets 1 and so on
        for i, b in enumerate(row):
            b.x += bump * i
    else:
        bump = slack / len(stretchies)
        # Each stretchy gets wider
        for b in stretchies:
            b.w += bump
        # And we put each thing next to the previous one
        for j, b in enumerate(row[1:], 1):
            b.x = row[j - 1].x + row[j - 1].w + separation

```

We can also do a simple function to add hyphens to a row if needed:

<div class='source_title'><a href="code/lesson3/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&87
def add_hyphen(row, separation):
    """If the row requires a hyphen at the end, add it, respecting separation.

    Returns the added hyphen or None."""
    h_b = None
    if row[-1].letter == "\xad":
        # Add a visible hyphen in the row
        h_b = hyphenbox()
        h_b.x = row[-2].x + row[-2].w + separation
        h_b.y = row[-2].y
        row.append(h_b)  # So it's justified
    return h_b

```

And change the code in `layout` so it uses those functions:

<div class='source_title'><a href="code/lesson3/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&144
            h_b = add_hyphen(row, separation)
            if h_b:
                _boxes.append(h_b)  # So it's drawn
            break_line = True
            justify_row(row, pages[page], separation)

```

And does it work? The same as before.

```plaintext
|||1
$ python boxes.py pride-and-prejudice.txt lesson3.svg
```

![lesson3.svg](lesson3.svg)

That will be enough for now. Our `layout` function is down to 76 lines, which
is in the upper limit of acceptable (aim for fewer than 50 lines if you can).

It may seem like we are just spinning our wheels here and not doing anything
useful. After all, our output is the same for several lessons! Don't worry, in
the next lesson, you will see why we did this.

What we have been doing is called [refactoring](https://en.wikipedia.org/wiki/Code_refactoring)
and is important. It could be defined as doing changes in the code that don't
affect its behavior. That the output has not changed *is the whole point*. We
are doing these changes not because they fix bugs but because they improve our
code so finding and fixing those bugs is easier from now on.

Before, if we wanted to change the way we justified lines, we would do it
inside `layout` and any silly mistake, (for example, indenting something wrong)
could break everything. Now it's isolated in a nice little corner of our code.

And that little function is going to be worked on.

----------

Further references:

* Full source code for this lesson <a href="boxes.py.html" target="_blank">boxes.py</a>
* <a href="code/diffs/lesson3_diff.html" target="_blank">Difference with code from last lesson</a>
