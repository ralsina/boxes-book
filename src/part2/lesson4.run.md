# Boxes v0.15

In the last few lessons, we have been doing refactoring of our code. We claim
it works the same based on an eyeball test: We run it before, we run it later,
it looks the same, we claim behavior hasn't changed.

In the lingo of programmers that's called "manual testing" and it has a bad
reputation, mostly because it takes a while to do. We have not looked exactly
carefully, mostly just check that it looks more or less the same. As we start
now the process of fixing bugs which are going to be more and more subtle,
that will not be good enough.

We need automated testing. We will now add that using [pytest](pytest.org), a
nifty tool to write and run automated tests on our code.

We will begin with the `justify_row` function. By convention, tests go in a
folder called `tests/` in files called `test_something.py` and contain
functions called `test_stuff`

Here is the code of that function as shown in the last lesson:

<div class='source_title'><a href="code/lesson3/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&59
def justify_row(row, page, separation):
    """Given a row and a page, adjust position of elements in the row
    so it fits the page width.

    It modifies the contents of the row in place, so returns nothing.
    """

    # Remove all right-margin spaces
    while row[-1].letter == " ":
        row.pop()
    slack = (page.x + page.w) - (row[-1].x + row[-1].w)
    # Get a list of all the ones that are stretchy
    stretchies = [b for b in row if b.stretchy]
    if not stretchies:  # Nothing stretches do as before.
        bump = slack / len(row)
        # The 1st box gets 0 bumps, the 2nd gets 1 and so on
        for i, b in enumerate(row):
            b.x += bump * i
    else:
        bump = slack / len(stretchies)
        # Each stretchy gets wider
        for b in stretchies:
            b.w += bump
        # And we put each thing next to the previous one
        for j, b in enumerate(row[1:], 1):
            b.x = row[j - 1].x + row[j - 1].w + separation

```

So here is our first test! A good test has 4 parts:

1. setup
2. precheck
3. execution
4. postcheck


<div class='source_title'><a href="code/lesson4/tests/test_justify_row.py.html" target="_blank">test_justify_row.py</a></div>

```python
&&&1
import boxes


def test_justify_simple():
    """Test a simple use case."""
    # First setup what we will use, our "test case"
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    page = boxes.Box(w=50, h=50)
    separation = .1

    # Check expected characteristics
    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    # Do the thing
    boxes.justify_row(row, page, separation)

    # Check the expected behavior

    # Should have the same number of elements
    assert len(row) == 10
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should be flushed-right
    assert row[-1].x + row[-1].w == page.x + page.w

```

To run the test, we use the `pytest` command. The use of `env` is an artifact
of this project not being in a proper directory structure yet, please
disregard.

```plaintext
|||1
$ env PYTHONPATH=. pytest

============================= test session starts ==============================
platform linux -- Python 3.6.4, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: code/lesson4, inifile:
collected 1 item

tests/test_justify_row.py F                                               [100%]

=================================== FAILURES ===================================
_____________________________ test_justify_simple ______________________________

    def test_justify_simple():
        """Test a simple use case."""
        # First setup what we will use, our "test case"
        row = [boxes.Box(x = i, w=1, h=1, letter='a') for i in range(10)]
        page = boxes.Box(w=50, h=50)
        separation = .1

        # Check expected characteristics
        assert len(row) == 10
        assert row[-1].x + row[-1].w == 10

        # Do the thing
        boxes.justify_row(row, page, separation)

        # Check the expected behavior

        # Should have the same number of elements
        assert len(row) == 10
        # The first element should be flushed-left
        assert row[0].x == page.x
        # The last element should be flushed-right
>       assert row[-1].x + row[-1].w == page.x + page.w
E       assert (45.0 + 1) == (0 + 50)
E        +  where 45.0 = Box(45.0, 0, 1, 0, "a").x
E        +  and   1 = Box(45.0, 0, 1, 0, "a").w
E        +  and   0 = Box(0, 0, 50, 0, "x").x
E        +  and   50 = Box(0, 0, 50, 0, "x").w

tests/test_justify_row.py:24: AssertionError
=========================== 1 failed in 0.29 seconds ===========================
```

That is a lot, because pytest tries to leave nothing to your imagination. The
short version: we had only one test, and it failed. When we expected the last
element in the row to be flush to the right side of the page, in position 50,
it's only in position 46.

So, hey, we have a bug! Here is how this process works:

1. Write code
2. Write a test that describes expected behavior
3. Run the test:

   * If it fails, fix it
   * If it didn't fail, pat yourself on the back

5. Repeat from 1. or 2.

So, we have 1. and 2. done, and we tried it, and it failed hard. So, we have
to fix it.

Because we are passing a row with no spaces, this is the "no stretchies" part
of `justify_row`:

<div class='source_title'><a href="code/lesson3/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&72
    if not stretchies:  # Nothing stretches do as before.
        bump = slack / len(row)
        # The 1st box gets 0 bumps, the 2nd gets 1 and so on
        for i, b in enumerate(row):
            b.x += bump * i

```

Why is it failing? I don't know. Yet. Guessing is often useless and it's
easier to just look. The general idea is that `slack` is to be evenly spread
by bumping each character a little to the right.

Let's just use a debugger. If you use the `--pdb` option, `pytest` will stop
when an assertion fails and leave you in a debugger prompt.

```plaintext
|||1
$ env PYTHONPATH=. pytest --pdb

[skipping stuff]

>       assert row[-1].x + row[-1].w == page.x + page.w
E       assert (45.0 + 1) == (0 + 50)
E        +  where 45.0 = Box(45.0, 0, 1, 0, "a").x
E        +  and   1 = Box(45.0, 0, 1, 0, "a").w
E        +  and   0 = Box(0, 0, 50, 0, "x").x
E        +  and   50 = Box(0, 0, 50, 0, "x").w

tests/test_justify_row.py:24: AssertionError
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> entering PDB >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
> tests/test_justify_row.py(24)test_justify_simple()
-> assert row[-1].x + row[-1].w == page.x + page.w

(Pdb)
```

So, let's examine things.

```plaintext
(Pdb) row[0].x
0.0
(Pdb) row[1].x
5.0
(Pdb) row[2].x
10.0
```

Hmmmm that looks like the `bump` we are using is 4. Since bump is `slack / len(row)`
that means slack is 40. That is good.

However, since we have to spread the slack by moving **9** boxes, that means
we are only moving the last box by `9 * 4` units, which is 36. If we add the
10 units we were already using, that means the last box will only reach
position 46. Which is what we saw in the test!

So, it looks like an off-by-one error. We actually want bump to be larger:

<div class='source_title'><a href="code/lesson4/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&72
    if not stretchies:  # Nothing stretches do as before.
        bump = slack / (len(row) - 1)
        # The 1st box gets 0 bumps, the 2nd gets 1 and so on
        for i, b in enumerate(row):
            b.x += bump * i

```

And we rerun the test:

```plaintext
env PYTHONPATH=. pytest
============================= test session starts ==============================
platform linux -- Python 3.6.4, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: code/lesson4, inifile:
collected 1 item

tests/test_justify_row.py .                                               [100%]

=========================== 1 passed in 0.24 seconds ===========================
```

And it works! We have just written our first test, found a bug, and fixed it.

Can we see the bug in action "In Real Life"? I don't know. Maybe? Creating a
test file that exhibits this specific behavior, a line with no spaces that has
to be justified into a whole row is not trivial. It probably *would* happen if
enough people fed enough text to the layout engine, in the spirit of the
infinite monkeys thing. But it would probably never happen where **we** could
see it.

This specific bug would be too hidden by other behavior to be obvious. It
would be "one of those things"... And that is one of the benefits of automated
tests. We can just write a test, and from now on, it will **have** to pass,
and this will **not** break again.


> **Info**
> Just as a funny note, I was not expecting this test to fail. This is a
> real bug I just found while writing this. So, if you ever run into a bug
> you wrote, don't feel bad, at least it's not in code you are giving people
> as an example of how to code.

In the next lesson, we will do this a few more times.

------

* Full source code for this lesson <a href="boxes.py.html" target="_blank">boxes.py</a>
* <a href="code/diffs/lesson4_diff.html" target="_blank">Difference with code from last lesson</a>
