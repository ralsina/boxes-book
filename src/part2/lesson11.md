# Boxes v0.22

In the [previous lesson](lesson10.run.html), we reimplemented our line breaking
algorithm and made it capable of choosing the best possible breaking point
giving constraints on what characters are allowable as breaking points and
page width.

If you want to get technical and improve the algorithm there is literature
about it. The next big step would be not breaking lines independently of each
other but to instead consider the paragraph to be the natural unit and try to
keep the "tightness" of ourspacing consistent across it.

That change is what Knuth proposes in his historical paper [Breaking Paragraphs into Lines](http://www.eprg.org/G53DOC/pdfs/knuth-plass-breaking.pdf)
...which we are not going to ever consider here. It would be **possible** to
tweak our code in that direction. But we are trying to learn something and our
text layout engine is, really, a bit of a [MacGuffin.](https://en.wikipedia.org/wiki/MacGuffin)

What I am trying to teach here is not how to do a **text layout engine**, but
**how to do** a text layout engine. The important part is the how, not the
what.

So, having duly noted that there is vast room for improvement in the
algorithm, we will turn to one last pass of improvements in our code.

Here is our `fill_row()` as of lesson 10:

```python-include-norun:../lesson10/boxes.py:117:151
```

It's basically two loops:

1. Calculate the 1st overfull breaking point
2. See if any previous breaking point is better

We **could** make it a single loop. We would track each possible breaking
point's badness as we reach it, and then when we are overfull pick the minimum
badness, and so on.

That would be a speed improvement. An optimization.

Faster is better. But it's not the **only** thing that is better. It's not
true that everything that is faster is better. The following things are also
better:

* Readable
* Easy to change in the future
* Easy to test
* Easy to debug

And (in my opinion) making it a single loop hurts all of those things. There
is a saying in development: "premature optimization is the root of all evil"
which, like all absolutes, is not true, but it is **mostly true**.

I prefer an alternative version: "It's easier to make correct code faster than
to make fast code correct."

So, let's work on making it correct. Then, maybe, if it's slow, we will make
it fast.

> **Info**
>
> Just for fun, I had this code typeset **all** of Pride and Prejudice.
> It took about 5 minutes. It also created a 72MB SVG file that took 20GB of
> RAM and 10 minutes to open using Inkscape.
>
> For comparison, pdfLaTeX with the most basic template (no packages at all)
> took less than a second to produce a 720KB PDF file.
>
> Does that mean our code is slow? Yes and no.
>
> Yes, because we have evidence that a similar thing can be done much faster.
> No, because we are not Knuth, he took years to do it, and we have not optimized anything.
>
> In fact, here's what Knuth had to say on the first day TeX produced a page:
>
> > Came in evening after sleeping most of day, to get computer at better time. Some day we will have personal computers and will live more normally.
> >
> > — D. E. Knuth, [The Errors of TeX](http://texdoc.net/texmf-dist/doc/generic/knuth/errata/errorlog.pdf), 14 March 1978

First: There is a whole thing in that code about `_row` and `row` that
looks *so bad* and confusing. Just renaming things can make code better:

```python-norun
&&&135
    # Calculate badness for previous breaking points
    result = row
    badnesses = {}
    for i in range(1, len(row)):
        partial_row = row[:i + 1]
        if partial_row[-1].letter in [" ", "\xad", "\n"]:
            how_bad = badness(page.w, partial_row)
            badnesses[how_bad] = partial_row
    if badnesses:
        # Find minimum badness
        min_bad = min(badnesses.keys())
        partial_row = badnesses[min_bad]
        # Put leftover letters back in boxes
        for b in row[:len(partial_row) - 1:-1]:
            boxes.insert(0, b)
        result = partial_row
    return result
```

Another code smell is this hardcoded list:

```python-include-norun:../lesson10/boxes.py:139:139
```

That needs to be a constant. We should add `'\n'` to our list of
`BREAKING_CHARS` and use it.

There is also this strange code:

```python-include-norun:../lesson10/boxes.py:146:148
```

Why is that code there? Because `fill_row()` is supposed to consume `boxes`
and move the consumed elements into `row` and return that, and leave the
unconsumed ones for the next iteration.

Since we are "overeating" we need to put those extra boxes back in the pot,
and there is no equivalent to `extend()` that would let us re-insert them at
the beginning of the `boxes` list.

The real problem is that we don't want a `list`, we want a `deque`. Choosing
the right data structures for your data is **important**.

The `deque` has the advantage that it allows for fast pops and appends on both
ends of the container.

So, if `boxes` were a `deque` then we could just use `extendleft`:

```python-norun
boxes.extendleft(reversed(row[len(partial_row):]))
```

In `layout()` instead of creating a copy of `boxes` we can create a `deque`:

```python-include-norun:boxes.py:154:158
```

When you change a data structure, it will trigger a propagation of changes
across your code. You can see what happened in <a
href="code/diffs/lesson11_diff.html" target="_blank">our code diff.</a>

Was it worth it? Dubious. While a `deque` is indeed the proper data structure,
it's also not very well known, and the next person looking at the code will be
surprised because it looks a lot like a list but *is not a list*.

We "fixed" ugly code by making other code slightly more complicated and
surprising than before. That is a tradeoff we should be aware of. I will leave
it there but we could just as easily not have done it.

Trade offs of that sort happen all the time. Every time you add a feature, or
fix a bug, or refactor something, you are paying a cost, and you are getting a
payoff. It's on you to get payoffs that offset your costs. You will **suck**
at it for a long time, and in the end it's a subjective matter and nobody has
the truth. Just don't stress too much over it.

We can fix more things. I have left you subtle hints in the code:

```python-include-norun:boxes.py:78:82
```

```python-include-norun:boxes.py:54:56
```

But let's just try to see if we have obvious bugs, instead. At one point, when we introduced the ability of specifying page sizes that exposed a lot of issues.

It looked like this:

```bash
$ python code/lesson2/boxes.py pride-and-prejudice.txt lesson2.svg --page-size=10x20
```

![lesson2.svg](lesson2.svg)

```bash
$ python code/lesson11/boxes.py pride-and-prejudice.txt lesson11.svg --page-size=10x20
```

![lesson11.svg](lesson11.svg)

That looks better! No overflows, no strangely dark smushed lines... but...
look at the vertical spacing. Before, our rows were flush to the top of the
page, and now there is more space there.

Looking back, that change seems to have happened a while ago, in [lesson 8](lesson8.run.html). That is interesting, because I honestly did not notice
until now. **Bugs will be unnoticed.** Yes, even large bugs. Even very large
bugs. It **will** happen to you.

Also, if we render with the normal page sizes, you can see that the bottom row
on each page is actually partly out of the page!

```bash
$ python code/lesson11/boxes.py pride-and-prejudice.txt lesson11.1.svg
```

![lesson11.1.svg](lesson11.1.svg)

So, yes, we have an obvious bug. Let's fix it. Remember TDD from [lesson 9](lesson9.run.html)? Let's create a test that exposes the bug.

```python-include-norun:tests/test_layout.py
```

It does indeed fail!

```text
env PYTHONPATH=. pytest tests/test_layout.py
============================= test session starts ==============================
platform linux -- Python 3.6.2, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: lesson11, inifile:
collected 1 item

tests/test_layout.py F                                                   [100%]

=================================== FAILURES ===================================
______________________________ test_page_overflow ______________________________

tmpdir = local('/tmp/pytest-of-ralsina/pytest-25/test_page_overflow0')

    def test_page_overflow(tmpdir):
        """When laying down text, it should not extend below the page."""

        # Create a normal text layout
        pages = boxes.create_pages((30,50))
        text = lipsum * 20
        inp = tmpdir.mkdir('sub').join('lipsum.txt')
        inp.write(text)
        text_boxes = boxes.create_text_boxes(inp)

        boxes.layout(text_boxes, pages, 0.05)

        max_y = max(b.y + b.h for b in text_boxes)
>       assert max_y < pages[0].h
E       assert 50.349999999999966 < 50
E        +  where 50 = Box(0, 0, 30, 50, "x").h

tests/test_layout.py:28: AssertionError
=========================== 1 failed in 0.42 seconds ===========================
```

So, we **are** laying down text that goes below the lower edge of the page.

The fix is pretty simple, and it was just a silly bug. It looked like this:

```python-include-norun:boxes.py:175:178
```

And it should have looked like this:

```python-include-norun:../lesson11.1/boxes.py:175:178
```

Is it fixed?

```bash
$ python code/lesson11.1/boxes.py pride-and-prejudice.txt lesson11.2.svg
```

![lesson11.2.svg](lesson11.2.svg)

Yes, yes it is.

And here, after 11 lessons, is where we stop. We have a decent text layout
engine here. Is it state of the art? Hell no. Is it fast? Probably not. Is it
great? Nope.

But we *understand it*. And we understand how to fix it, and how to improve
it. And it does something useful-looking! Do you know any other tools to
typeset SVG files out of text? I don't!

This is as good a time as any to move on. Let's do a quick recap!

------

* Full source code for this lesson <a href="code/lesson11/boxes.py.html"
  target="_blank">boxes.py</a>
* <a href="code/diffs/lesson11_diff.html" target="_blank">Difference with code
  from last lesson</a>
