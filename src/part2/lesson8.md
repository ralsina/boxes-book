# Boxes v0.19

We have spent several lessons ([2](lesson2.run.html) [3](lesson3.run.html)
[4](lesson4.run.html) [5](lesson5.run.html) [6](lesson6.run.html)
[7](lesson7.run.html)) improving our code.

We have turned it from a pile of functions into a working command line tool,
we have refactored a few things out of our most complex function, `layout()`
and into a few smaller, testable functions and we have created automated
tests for them.

Now the time has come to consider the elephant in the room. We said we
needed to add tests and do refactoring because `layout()` was too complex,
and we did. So we have to put our code on the table and fix bugs.

The main bug we have is that the choice of breaking points for rows is
not optimal. Often, when I have to make algorithmic changes in a function,
it helps to describe what it does in plain English.

If I can't, or the description is too long, that often means I need to
refactor further.

Here is the function as it is now:

```python-include-norun:../lesson7/boxes.py:121:179
```

And here is my attempt at describing it:

* We take a list of boxes and one of pages (and a letter separation measure)
* Starting at the first page's top-left , we lay boxes one next to the other
  in a row
* If we need to break a row:
  * We add a hyphen if needed
  * Justify the row
  * Start back from the left edge of the page
* If we have gone too far down and are off the page:
  * We Move the row to the top-left of the next page
  * Make that page our "current page"
* We collapse all left-margin space (WAT?)
* After we run out of boxes, we remove the unused pages

That is **not** too long. It does seem to point that we could split
`layout()` into an outer function handling pages and an inner one
filling a single page, but that's minor.

Also, the collapsing of left-margin space is in a weird place, but we
can ignore it.

Keep in mind that what we have there is a description of *what `layout()`
does now* and not necessarily of *what it should do*.

In fact, it seems clear that if we want to be able to choose the best
breaking point for a row, we need to rethink how this function works
and find a better way to do it. If the change is large enough,
refactoring the algorithmic change may require a full rewrite of the
function.

And that's why we did all the previous refactors extracting code out of
`layout()`. The more we extract, the less we have to replace.

Let's walk this path in the other direction. Let's speak about what `layout()`
does and turn *that* into code doing it.

On a very high level way, we could say that what we are doing is just breaking
text into lines and breaking the list of lines into pages. Therefore:

* We take a list of boxes and one of pages (and a letter separation measure)
* Find the best amount of boxes from our list of boxes we can fit in a page
  width.
  * Put that row in the top-left of the page or below the last row.
  * Repeat until we run out of page (or rows)
* Use the next page
  * Repeat until we are out of rows (or pages)

If you compare with the previous description, you will see that this code is
not in a single loop, so it will look pretty different. I don't think the new
`layout()` will have much in common with the old one... except that it should do
more or less the same thing.

In fact, if we made the same decision about how many boxes we put in a row, it
should behave *the same way* as the current version. Probably.

So, let's do it in three stages:

1. We implement the new code with the same line breaking decisions we have now.
2. We make sure we are not breaking something else
3. We improve those decisions.

> **Info**
>
> This may seem very conservative. Why not do both things at once?
>
> Well, if I were doing this on my own, I probably would. But that would make
> the change a bit hard to follow, so I am trading space for simplicity.

Let's start with code to "Find the best amount of boxes from our list of boxes
we can fit in a page width." This is supposed to be the same logic we had
before but refactored into a separate function just like we have already done
for other things:

* Pop letters one at a time.
* Put them in the row next to the last letter.
* If it's a good place to break, return that row.

```python-include-norun:boxes.py:115:132
```

BTW, I **did** write some tests for it, you can see them in
<a href="code/lesson8/tests/test_fill_row.py.html" target="_blank">test_fill_row.py</a>

And here is the new `layout()` in all its smallness:

```python-include-norun:boxes.py:135:161
```

I took code that added real hyphens at the end of rows broken on soft-hyphens
and moved it into `justify_row()`:

```python-include-norun:boxes.py:74:79
```

And of course added a test for it called `test_add_hyphen()` in <a
href="code/lesson8/tests/test_justify_row.py.html"
target="_blank">test_justify_row.py</a>. This means we are not using
`add_hyphen()` anywhere so I removed it.

Does it work?

```bash
$ python boxes.py ./pride-and-prejudice.txt lesson8.svg
```

![lesson8.svg](lesson8.svg)

Well, not perfectly. The last line is weirdly justified. But it's not bad for
such a drastic change!

The problem with the last line is a mystery. Before we improve our line-breaking
algorithm, we will have to fix it.

We may end up having some actual tests for `layout()` now!

------

* Full source code for this lesson <a href="boxes.py.html"
  target="_blank">boxes.py</a>
* <a href="code/diffs/lesson8_diff.html" target="_blank">Difference with code
  from last lesson</a>
