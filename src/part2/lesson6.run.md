# Boxes v0.17

In the [previous lesson](lesson5.html) we added tests for our `justify_row()`
function, allowing us to have some confidence that we will not break it if we
play with it.

In the lesson **before that** we complained that our `layout()` function was
too complex, and that was why we created `justify_row()` in the first place.

So, let's continue to take things out of `layout()`...

Look at these fragments of code:

<div class='source_title'><a href="code/lesson5/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&129
        break_line = False
        # But if it's a newline
        if (box.letter == "\n"):
            break_line = True
            # Newlines take no horizontal space ever
            box.w = 0
            box.stretchy = False

        # Or if it's too far to the right, and is a
        # good place to break the line...
        elif (box.x + box.w) > (
            pages[page].x + pages[page].w
        ) and box.letter in (
            " ", "\xad"
        ):
            h_b = add_hyphen(row, separation)
            if h_b:
                _boxes.append(h_b)  # So it's drawn
            break_line = True

```

<div class='source_title'><a href="code/lesson5/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&150
        if break_line:
            # We start a new row
            row = []
            # We go all the way left and a little down
            box.x = pages[page].x
            box.y = previous.y + previous.h + separation

```

What is happening is that we can `break_line` for two reasons, and thus are
setting a "flag" which we then use to do the actual line breaking. That is
because if we break because of a newline, we don't justify the line.

So, if we gave `justify_row()` the skill of handling lines ended in newline,
then that distinction goes away and layout() is simpler.

The good thing is that we already have a test for it! It's the one we marked
as "expected failure" in the previous lesson, so if we make it pass... we are
on the right path.

How do we want to handle it? Let's look at our test, and remember that *we are
defining the desired behavior there*.

<div class='source_title'><a href="code/lesson5/tests/test_justify_row.py.html" target="_blank">test_justify_row.py</a></div>

```python
&&&55
@pytest.mark.xfail  # FIXME: justify doesn't handle newlines yet!
def test_justify_ends_with_newline():
    """Test a use case with a newline."""
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    row[-1].letter = "\n"
    page = boxes.Box(w=50, h=50)
    separation = .1

    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    boxes.justify_row(row, page, separation)

    # Should have the same number of elements
    assert len(row) == 10
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should NOT be flushed-right
    assert row[-1].x + row[-1].w == 10

```

It looks easy enough: just **don't** spread the slack if we are ending on a
newline. That change is simple:

<div class='source_title'><a href="code/lesson6/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&59
def justify_row(row, page, separation):
    """Given a row and a page, adjust position of elements in the row
    so it fits the page width.

    It modifies the contents of the row in place, so returns nothing.
    """

    # Remove all right-margin spaces
    while row[-1].letter == " ":
        row.pop()
    # If the line ends in newline, do nothing.
    if row[-1].letter == "\n":
        return

```

And run the test suite.

```text
$ env PYTHONPATH=. pytest
============================= test session starts ==============================
platform linux -- Python 3.6.2, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: code/lesson6, inifile:
collected 5 items

tests/test_justify_row.py ..X..                                   [100%]

===================== 4 passed, 1 xpassed in 0.25 seconds ======================
```

And now our test "xpasses". That means we marked as "expected to fail" and it
unexpectedly passes.

So, we can remove that mark now, and the test suite passes. Let's change
`layout()` to make it do the right thing by removing code and changing the
`if` in line 133:

<div class='source_title'><a href="code/lesson6/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&132
        # If it's a newline or if it's too far to the right, and is a
        # good place to break the line...
        if (box.letter == "\n") or (box.x + box.w) > (
            pages[page].x + pages[page].w
        ) and box.letter in (
            " ", "\xad"
        ):
            h_b = add_hyphen(row, separation)
            if h_b:
                _boxes.append(h_b)  # So it's drawn
            justify_row(row, pages[page], separation)
            # We start a new row
            row = []
            # We go all the way left and a little down
            box.x = pages[page].x
            box.y = previous.y + previous.h + separation

        # But if we go too far down
        if box.y + box.h > pages[page].y + pages[page].h:
            # We go to the next page
            page += 1
            # And put the box at the top-left
            box.x = pages[page].x
            box.y = pages[page].y

        # Put the box in the row
        row.append(box)

```

But ... we are changing `layout()`. And `layout()` has no tests. We need to
manually test it.

```plaintext
|||1
$ python boxes.py pride-and-prejudice.txt lesson6.svg
```

![lesson6.svg](lesson6.svg)

Oh, no, it's broken!

Can you guess what happened?

In the lines that end in newlines (like the first one), we are *still*
spreading the slack! That's because we are only adding the current box to the
row after we justify it, in line 158!

If we move that up, before the `if` that decides whether to break the line,
then it works:

<div class='source_title'><a href="code/lesson6.1/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&129
        # Put the box in the row
        row.append(box)

        # If it's a newline or if it's too far to the right, and is a
        # good place to break the line...
        if (box.letter == "\n") or (box.x + box.w) > (
            pages[page].x + pages[page].w
        ) and box.letter in (
            " ", "\xad"
        ):

```

```plaintext
|||1
$ python ../lesson6.1/boxes.py pride-and-prejudice.txt lesson6.1.svg
```

![lesson6.1.svg](lesson6.1.svg)

While we are here, let's change one more thing. Look at this code in
`justify_row()`:

<div class='source_title'><a href="code/lesson6/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&76
    if not stretchies:  # Nothing stretches do as before.
        bump = slack / (len(row) - 1)
        # The 1st box gets 0 bumps, the 2nd gets 1 and so on
        for i, b in enumerate(row):
            b.x += bump * i
    else:
        bump = slack / len(stretchies)
        # Each stretchy gets wider
        for b in stretchies:
            b.w += bump
        # And we put each thing next to the previous one
        for j, b in enumerate(row[1:], 1):
            b.x = row[j - 1].x + row[j - 1].w + separation

```

* If we have `stretchies` then we make them wider.
* If we don't, then we move every box a little to the right.

Why are we doing this, which is the same thing, "spreading the slack" in two
different ways?

No reason.

So, because we have tests for `justify_row()` we can make the code more
**uniform** and less complex.

<div class='source_title'><a href="code/lesson6.1/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&76
    if not stretchies:
        # Nothing stretches, spread slack on everything
        stretchies = row
    bump = slack / len(stretchies)
    # Each stretchy gets wider
    for b in stretchies:
        b.w += bump
    # And we put each thing next to the previous one
    for j, b in enumerate(row[1:], 1):
        b.x = row[j - 1].x + row[j - 1].w + separation

```

```text
$ env PYTHONPATH=. pytest

============================= test session starts ==============================
platform linux -- Python 3.6.2, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: code/lesson6.1, inifile:
collected 5 items
tests/test_justify_row.py F..F.                                           [100%]

=================================== FAILURES ===================================
_____________________________ test_justify_simple ______________________________

[snip]
        # The last element should be flushed-right
>       assert row[-1].x + row[-1].w == page.x + page.w
E       assert (45.900000000000006 + 5.0) == (0 + 50)
E        +  where 45.900000000000006 = Box(45.900000000000006, 0, 5.0, 0, "a").x
E        +  and   5.0 = Box(45.900000000000006, 0, 5.0, 0, "a").w
E        +  and   0 = Box(0, 0, 50, 0, "x").x
E        +  and   50 = Box(0, 0, 50, 0, "x").w

tests/test_justify_row.py:27: AssertionError
_________________________ test_justify_trailing_spaces _________________________

[snip]
        # The last element should be flushed-right
>       assert row[-1].x + row[-1].w == page.x + page.w
E       assert (44.45 + 6.25) == (0 + 50)
E        +  where 44.45 = Box(44.45, 0, 6.25, 0, "a").x
E        +  and   6.25 = Box(44.45, 0, 6.25, 0, "a").w
E        +  and   0 = Box(0, 0, 50, 0, "x").x
E        +  and   50 = Box(0, 0, 50, 0, "x").w

tests/test_justify_row.py:93: AssertionError
====================== 2 failed, 3 passed in 0.21 seconds ======================
```

Annnnnd it fails horribly. That is **good**. What did you expect? That we
could just change the code and everything would be **fine?**

No, reader, that is not how this works. This is good because our tests
found a bug we just introduced. We went in with an ax, we broke
something. We now fix it. Hakuna matata, the circle of coding.

Turns out the problem was a bug in our tests. We were creating the rows
like this:

```python
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    page = boxes.Box(w=50, h=50)
    separation = .1
```

And that is wrong, because we are saying they have a `separation` of .1 and
**are not separating them**.

The fix is easy:


<div class='source_title'><a href="code/lesson6.2/tests/test_justify_row.py.html" target="_blank">test_justify_row.py</a></div>

```python
&&&9
    separation = .1
    row = [
        boxes.Box(x=i + i * separation, w=1, h=1, letter="a")
        for i in range(10)
    ]

```

You think about it :-)

And, does it look good in real life?

```plaintext
|||1
$ python ../lesson6.2/boxes.py pride-and-prejudice.txt lesson6.2.svg
```

![lesson6.2.svg](lesson6.2.svg)

------

* Full source code for this lesson <a href="../lesson6.2/boxes.py.html" target="_blank">boxes.py</a>
* <a href="code/diffs/lesson6_diff.html" target="_blank">Difference with code from last lesson</a>
