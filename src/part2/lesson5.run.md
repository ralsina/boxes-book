# Boxes v0.16

In the [previous lesson](lesson4.run.html) we created a test with some basic
behavior we expected, it failed, we fixed the code, and it passed. Nice!

We specifically tested the part in `justify_row` that handles lines without
any spaces or hyphens. Let's add more tests to see if `justify_row` does the
right thing. This is just thinking behavior and creating tests for it.

Let's try some very basic behavior we want when there are spaces:

If we feed "aaaaa aaaaa" to `justify_row` we expect to have the row fully
justified, and the space in the middle to have grown a lot.

<div class='source_title'><a href="code/lesson5/tests/test_justify_row.py.html" target="_blank">test_justify_row.py</a></div>

```python
&&&30
def test_justify_with_spaces():
    """Test a simple use case with spaces."""
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    row[5].letter = " "
    row[5].stretchy = True
    page = boxes.Box(w=50, h=50)
    separation = .1

    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    boxes.justify_row(row, page, separation)

    # Should have the same number of elements
    assert len(row) == 10
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should be flushed-right
    # Use approx() because floating point adds a tiny error here
    assert pytest.approx(row[-1].x + row[-1].w) == page.x + page.w
    # The element in position 5 must have absorbed all the slack
    # So is 1 (it's width) + 40 (slack) units wide
    assert row[5].w == 41

```

We can tell `pytest` to run only this specific test:

```plaintext
|||1
$ env PYTHONPATH=. pytest tests/test_justify_row.py::test_justify_with_spaces

============================= test session starts ==============================
platform linux -- Python 3.6.4, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: code/lesson5, inifile:
collected 1 item

tests/test_justify_row.py .                                               [100%]

=========================== 1 passed in 0.24 seconds ===========================
```

Nice!

How about other behaviors? Our function removes spaces from the right:

<div class='source_title'><a href="code/lesson5/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&66
    # Remove all right-margin spaces
    while row[-1].letter == " ":
        row.pop()

```

Let's do a test to make sure we do that!

<div class='source_title'><a href="code/lesson5/tests/test_justify_row.py.html" target="_blank">test_justify_row.py</a></div>

```python
&&&76
def test_justify_trailing_spaces():
    """Test a use case with traling spaces."""
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    row[-1].letter = " "
    row[-2].letter = " "
    page = boxes.Box(w=50, h=50)
    separation = .1

    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    boxes.justify_row(row, page, separation)

    # Should have lost the 2 trailing spaces
    assert len(row) == 8
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should be flushed-right
    assert row[-1].x + row[-1].w == page.x + page.w

```

That works too!

How about lines with a newline character at the end? If we have "aaa aaa
aaa\n" then that should not actually be justified, right?

<div class='source_title'><a href="code/lesson5/tests/test_justify_row.py.html" target="_blank">test_justify_row.py</a></div>

```python
&&&56
def test_justify_ends_with_newline():
    """Test a use case with a newline."""
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    row[-1].letter = "\n"
    page = boxes.Box(w=50, h=50)
    separation = .1

    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    boxes.justify_row(row, page, separation)

    # Should have the same number of elements
    assert len(row) == 10
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should NOT be flushed-right
    assert row[-1].x + row[-1].w == 10

```

```plaintext
|||1
$ env PYTHONPATH=. pytest tests/test_justify_row.py::test_justify_ends_with_newline

[skipping]

        # The last element should NOT be flushed-right
>       assert row[-1].x + row[-1].w == 10
E       assert (49.0 + 1) == 10
E        +  where 49.0 = Box(49.0, 0, 1, 0, "\n").x
E        +  and   1 = Box(49.0, 0, 1, 0, "\n").w

tests/test_justify_row.py:68: AssertionError
```

And it fails. That is not surprising because if you look at our implementation
of `justify_row()` it doesn't check for newlines at all! That is because we
are handling that case in `layout()` instead. This is arguably correct. On the
other hand, if we want to encapsulate the justification of a row in this
function it makes sense to move that here.

At this point, however, that is not implemented but it's not really a bug
because it *is* handled. So, we can mark this test as an "expected failure"
and put a pin in it so we can come back to it. We mark it as expected failure
using the `pytest.mark.xfail` decorator, and add a `FIXME` comment to remember
to come back.

<div class='source_title'><a href="code/lesson5/tests/test_justify_row.py.html" target="_blank">test_justify_row.py</a></div>

```python
&&&55
@pytest.mark.xfail  # FIXME: justify doesn't handle newlines yet!
def test_justify_ends_with_newline():
    """Test a use case with a newline."""

```

The other thing this function does is put everything in an actual row
considering "separation":

<div class='source_title'><a href="code/lesson5/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&82
        # And we put each thing next to the previous one
        for j, b in enumerate(row[1:], 1):
            b.x = row[j - 1].x + row[j - 1].w + separation

```

Let's test if it does indeed do that!

<div class='source_title'><a href="code/lesson5/tests/test_justify_row.py.html" target="_blank">test_justify_row.py</a></div>

```python
&&&97
def test_justify_puts_things_in_a_row():
    """Test a simple use case with spaces."""
    row = [boxes.Box(x=i, w=1, h=1, letter="a") for i in range(10)]
    row[5].letter = " "
    row[5].stretchy = True
    page = boxes.Box(w=50, h=50)
    separation = .1

    assert len(row) == 10
    assert row[-1].x + row[-1].w == 10

    boxes.justify_row(row, page, separation)

    # Should have the same number of elements
    assert len(row) == 10
    # The first element should be flushed-left
    assert row[0].x == page.x
    # The last element should be flushed-right
    # Use approx() because floating point adds a tiny error here
    assert pytest.approx(row[-1].x + row[-1].w) == page.x + page.w
    # All elements should be separated correctly.
    separations = [
        separation - (row[i].x - (row[i - 1].x + row[i - 1].w))
        for i in range(1, len(row))
    ]
    # Again, floating point is inaccurate
    assert max(separations) < 0.00001
    assert min(separations) > -0.00001

```

And we finish running our whole test suite and see what happens.

```plaintext
|||1
$ env PYTHONPATH=. pytest
============================= test session starts ==============================
platform linux -- Python 3.6.4, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: part2/code/lesson5, inifile:
collected 5 items

tests/test_justify_row.py ..x..                                           [100%]

===================== 4 passed, 1 xfailed in 0.37 seconds ======================
```

Notice how we have 4 passing tests and an `xfail`.

What we have been working on is called "test coverage". We now have tests that
cover the expected behavior of `justify_row`. That means that when, in the
next lesson, we **change** that behavior, we can be fairly sure we **know**
how we are changing it.

------

* Full source code for this lesson <a href="boxes.py.html" target="_blank">boxes.py</a>
* <a href="code/diffs/lesson5_diff.html" target="_blank">Difference with code from last lesson</a>
