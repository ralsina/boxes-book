# Boxes v0.20

In the [previous lesson](lesson8.run.html) we finally finished refactoring our
`layout()` function, by moving most of the code into separate independent
functions and rewrote what was left to allow for independent improvement of
its parts.

Want to change how we decide line breaks? We hack `fill_row()`. Want to change
the algorithm used to correct horizontal positioning of letters? We improve
`justify_row()`. Our code has come a long way!

On the other hand, we introduced a bug.

![lesson8.svg](lesson8.svg)

In the third page, in the last line, the text is separated strangely. Those
who are about my age probably have seen something like it on newspapers long
ago. It's a case of trying to justify a line that really should have been kept
left-justified.

There are not enough letters there to fill the whole width of the page, so
things get ugly and very separate.

Further, there is no *reason* to justify that line. It's the last one in the
text so it could just as easily be left-aligned like lines that end in
newlines do.

Our text does not, however, end in a newline. Therefore it attempts full
justification and fails badly.

The simplest solution is to make all text end with a newline. Here is the function that loads text files, `create_text_boxes()`:

<div class='source_title'><a href="code/lesson8/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&201
def create_text_boxes(input_file):
    p_and_p = open(input_file).read()
    p_and_p = insert_soft_hyphens(p_and_p)  # Insert invisible hyphens
    text_boxes = []
    for letter in p_and_p:
        text_boxes.append(Box(letter=letter, stretchy=letter == " "))
    adjust_widths_by_letter(text_boxes)
    return text_boxes

```

And the obvious fix:

<div class='source_title'><a href="code/lesson9/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&201
def create_text_boxes(input_file):
    p_and_p = open(input_file).read()
    p_and_p = insert_soft_hyphens(p_and_p)  # Insert invisible hyphens
    if p_and_p[-1] != "\n":
        p_and_p += "\n"
    text_boxes = []
    for letter in p_and_p:
        text_boxes.append(Box(letter=letter, stretchy=letter == " "))
    adjust_widths_by_letter(text_boxes)
    return text_boxes

```

And because we are now serious people: the tests.

<div class='source_title'><a href="code/lesson9/tests/test_create_test_boxes.py.html" target="_blank">test_create_test_boxes.py</a></div>

```python
&&&1
import boxes


def test_adds_newline(tmpdir):
    """Test that we add a newline if the input file doesn't end in one."""
    # Create a temporary file with context
    hello = tmpdir.mkdir("sub").join("hello.txt")
    hello.write("hello")
    # No newline
    assert hello.read() == "hello"

    text_boxes = boxes.create_text_boxes(hello)

    # And now we have a newline (and a soft hyphen)
    assert "".join([b.letter for b in text_boxes]) == "hel\xadlo\n"


def test_not_add_newline_if_repeated(tmpdir):
    """Test that we don't add a newline if the input file ends in one."""
    # Create a temporary file with context
    hello = tmpdir.mkdir("sub").join("hello.txt")
    hello.write("hello\n")
    # No newline
    assert hello.read() == "hello\n"

    text_boxes = boxes.create_text_boxes(hello)

    # And now we have a newline (and a soft hyphen)
    assert "".join([b.letter for b in text_boxes]) == "hel\xadlo\n"

```

If you look carefully at those tests, you will see some ... unusual things.

* What is that `tmpdir` argument? Where is it coming from?
* What *is* `hello`? Why can I write to it like a file?
* If I am creating a temporary file, where is it? Does it go away?

In order:

`tmpdir` is a [pytest fixture.](https://docs.pytest.org/en/latest/fixture.html) Think
of it as a helper. A fixture lets you easily set something up, use it, and
have it be automatically cleaned up after you are done.

Often, when writing tests, you need to test against a file. Using [tmpdir](https://docs.pytest.org/en/latest/tmpdir.html)
you can create as many files and directories as you want, and use them in your
tests.

They are created somewhere in your system where temporary files go, and they
are deleted when the test finishes.

Pytest has many other [fixtures for common uses](https://docs.pytest.org/en/latest/builtin.html)
and you can even create your own. Learning about the builting fixtures is
useful. There are also many other fixtures available on the Internet for
things such as working with databases.

Did we fix our problem?

```plaintext
|||1
$ python lesson9/boxes.py ./pride-and-prejudice.txt lesson9.svg
```

![lesson9.svg](lesson9.svg)

Yes! On the other hand, if you look a few lines higher, you can see a line
that says `You have no compassion for my poor ner` which in the original text
says `for my poor nerves."` with a quote at the end.

Looks like we are actually missing a piece of text!

Can we create a test case that shows the same problem without it being the
long text? Usually the smaller the sample that causes the problem, the easier
it is to debug it.

```plaintext
|||1,2
$ echo 'take delight in vexing me. You have no compassion for my poor nerves.”' > line.txt
$ python boxes.py ./line.txt lesson9.1.svg --page-size=30x3
```

![lesson9.1.svg](lesson9.1.svg)

So, it seems (to me) like we may have a hyphenation problem. Surely there is
some point in "nerves" where there could be a hyphen?

```python
>>> from hyphen import insert_soft_hyphens
>>> insert_soft_hyphens('nerves', hyphen='-')

'nerves'
```

Oops, looks like there isn't. But in this case, when we need to justify an
overfull line... we don't. So that means there is a bug in `justify_row()`?

Here is what we are going to do. Now that we are somewhat convinced there is a
bug, let's create a test that *shows* the bug. Then we fix the code so the test
passes. That is called TDD, or [Test Driven Development](https://en.wikipedia.org/wiki/Test-driven_development). You can read many books
about it, but the basic gist is:

* Find a bug
* Write a failing test
* Fix the code so the test doesn't fail
* Refactor any ugliness you introduced
* Repeat

We have the bug, here is the test:

<div class='source_title'><a href="code/lesson9/tests/test_justify_row.py.html" target="_blank">test_justify_row.py</a></div>

```python
&&&161
def test_justify_overfull(tmpdir):
    """If a line is overfull, it still should be justified."""

    separation = 0.05
    page = boxes.Box(0, 0, 30, 50)
    # Our failing text
    inp = tmpdir.mkdir("sub").join("hello.txt")
    inp.write(
        "take delight in vexing me. You have no compassion for my poor nerves.\”"
    )
    # Adjust widths:
    row = boxes.create_text_boxes(inp)
    # Put side by side
    boxes.fill_row(row[:], page, separation)

    # Should be overwide
    assert row[-1].x > page.w

    boxes.justify_row(row, page, separation)

    # Should now be justified
    assert row[-1].x + row[-1].w == pytest.approx(page.w)

```

And it does indeed fail:

```plaintext
|||1
$ env PYTHONPATH=. pytest tests/test_justify_row.py::test_justify_overfull

============================= test session starts ==============================
platform linux -- Python 3.6.4, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: part2/code/lesson9, inifile:
collected 1 item
tests/test_justify_row.py F                                              [100%]

=================================== FAILURES ===================================
____________________________ test_justify_overfull _____________________________

[snip]

        boxes.justify_row(row, page, separation)

        # Should now be justified
>       assert row[-1].x + row[-1].w == pytest.approx(page.w)
E       assert (32.68750000000003 + 0.671875) == 30 ± 3.0e-05
E        +  where 32.68750000000003 = Box(32.68750000000003, 0, 0.671875, 1, "\n").x
E        +  and   0.671875 = Box(32.68750000000003, 0, 0.671875, 1, "\n").w
E        +  and   30 ± 3.0e-05 = <function approx at 0x7f500f96d2f0>(30)
E        +    where <function approx at 0x7f500f96d2f0> = pytest.approx
E        +    and   30 = Box(0, 0, 30, 50, "x").w

tests/test_justify_row.py:182: AssertionError
=========================== 1 failed in 0.30 seconds ===========================
```

All that is left is fixing it, right?

One way to do this is to run that test and step through it using a debugger.
Here is a session trying to do that. Just put a breakpoint like this in the
test:

```python
import pdb; pdb.set_trace()
```

```plaintext
|||1
$ env PYTHONPATH=. pytest tests/test_justify_row.py::test_justify_overfull
============================= test session starts ==============================
platform linux -- Python 3.6.4, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: code/lesson9, inifile:
collected 1 item

tests/test_justify_row.py
>>>>>>>>>>>>>>>>>>> PDB set_trace (IO-capturing turned off) >>>>>>>>>>>>>>>>>>>>
> tests/test_justify_row.py(164)test_justify_overfull()
-> separation = 0.05
(Pdb) n
```

Here, `n` means "run the next line".

```plaintext
> tests/test_justify_row.py(165)test_justify_overfull()
-> page = boxes.Box(0, 0, 30, 50)
(Pdb) n
> tests/test_justify_row.py(167)test_justify_overfull()
-> inp = tmpdir.mkdir("sub").join("hello.txt")
(Pdb) n
> tests/test_justify_row.py(168)test_justify_overfull()
-> inp.write(
(Pdb) n
> tests/test_justify_row.py(169)test_justify_overfull()
-> "take delight in vexing me. You have no compassion for my poor nerves.\”"
(Pdb) n
> tests/test_justify_row.py(172)test_justify_overfull()
-> row = boxes.create_text_boxes(inp)
(Pdb) n
> tests/test_justify_row.py(174)test_justify_overfull()
-> boxes.fill_row(row[:], page, separation)
(Pdb) n
> tests/test_justify_row.py(177)test_justify_overfull()
-> assert row[-1].x > page.w
(Pdb) n
> tests/test_justify_row.py(179)test_justify_overfull()
-> boxes.justify_row(row, page, separation)
(Pdb) s
```

However `s` means "step into the next line". We are going to go *inside*
`justify_row()`

```
--Call--
> boxes.py(59)justify_row()
-> def justify_row(row, page, separation):
(Pdb) n
> boxes.py(67)justify_row()
-> while row[-1].letter == " ":
(Pdb)
> boxes.py(71)justify_row()
-> if row[-1].letter == "\n":
(Pdb)
> boxes.py(72)justify_row()
-> return
(Pdb)
--Return--
```

Oh. So, it's not justifying the row because it ends in a newline. Looks like
we **do** have a bug. We want to justify the rows that end in newline *if they
are wider than the page*.

So here is the fix:

<div class='source_title'><a href="code/lesson9.1/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&59
def justify_row(row, page, separation):
    """Given a row and a page, adjust position of elements in the row
    so it fits the page width.

    It modifies the contents of the row in place, so returns nothing.
    """

    # Remove all right-margin spaces
    while row[-1].letter == " ":
        row.pop()

    # If the line ends in newline, and is underfull, do nothing.
    if row[-1].letter == "\n" and (row[-1].x + row[-1].w) < (
        page.x + page.w
    ):
        return

```

Which makes tests pass, and produces this output:

```plaintext
|||1,2
$ echo 'take delight in vexing me. You have no compassion for my poor nerves.”' > line.txt
$ python lesson9.1/boxes.py ./line.txt lesson9.2.svg --page-size=30x3
```

![lesson9.2.svg](lesson9.2.svg)

Which is bad. But in a different way. Clearly, we are breaking the line in a
bad spot. We have mentioned before that our choice of breaking points sucks.
Overfull lines are *bad* and justifying them by smushing things together looks
*bad*.

In fact, it's different in **two** ways.

Besides the smushing, it doesn't look justified. There is space on the
right! But our test says it's justified!

Not to keep you guessing, it turns out harfbuzz is giving newlines width.
I have no idea if that is correct or wrong technically, but I know it's
wrong for us.

One last fix: make sure newlines have zero width.

<div class='source_title'><a href="code/lesson9.2/fonts.py.html" target="_blank">fonts.py</a></div>

```python
&&&1
import harfbuzz as hb
import freetype2 as ft


def adjust_widths_by_letter(boxes):
    """Takes a list of boxes as arguments, and uses harfbuzz to
    adjust the width of each box to match the harfbuzz text shaping."""
    buf = hb.Buffer.create()
    buf.add_str("".join(b.letter for b in boxes))
    buf.guess_segment_properties()
    font_lib = ft.get_default_lib()
    face = font_lib.find_face("Arial")
    face.set_char_size(size=1, resolution=64)
    font = hb.Font.ft_create(face)
    hb.shape(font, buf)
    # at this point buf.glyph_positions has all the data we need
    for box, position in zip(boxes, buf.glyph_positions):
        # For us, newlines should have zero width
        if box.letter != "\n":
            box.w = position.x_advance
        else:
            box.w = 0

```

And a final visual check:

```plaintext
|||1,2
$ echo 'take delight in vexing me. You have no compassion for my poor nerves.”' > line.txt
$ python lesson9.2/boxes.py ./line.txt lesson9.3.svg --page-size=30x3
```

![lesson9.3.svg](lesson9.3.svg)

```plaintext
|||1
$ python lesson9.2/boxes.py ./pride-and-prejudice.txt lesson9.4.svg
```

![lesson9.4.svg](lesson9.4.svg)


And next lesson we will choose our breaking points better.

------

* Full source code for this lesson <a href="../lesson9.1/boxes.py.html"
  target="_blank">boxes.py</a>
* <a href="code/diffs/lesson9_diff.html" target="_blank">Difference with code
  from last lesson</a>
