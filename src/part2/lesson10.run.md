# Boxes v0.21

In the [previous lesson](lesson9.run.html), we looped another loop in the
process of iteratively improving our code. After we did a large refactor
in [lesson 7](lesson7.run.html) we had to pay the price by fixing a
couple of bugs we had introduced, and while doing it, we learned some
new techniques.

That is pretty much what software development is. We make a push to get a
feature implemented. We found a bug or two. We add tests and fix the bugs. We
add another feature.

So now it's feature time. We want to choose our line breaks "better". But what
does "better" mean in this context? A while ago we introduced a function
called `badness()` to measure how *bad* a breaking point was. So, better could
mean "has a smaller badness".

Without getting into the particulars of *how* we measure badness, this sounds
reasonable, or at least makes intutitive sense to me:

* Badness starts very high.
* Badness decreases as we add text to the row.
* At some point, badness starts increasing again.
* Badness will then tend to grow forever as long as we add text.

Consider the following line breaks to see why this is the case. `|` indicates
the right edge of the page, so breaking there would be ideal.

```python
The                            |              # Very bad
The quick                      |              # Slightly less bad
The quick brown                |              # Still bad
The quick brown fox            |              # Not so bad
The quick brown fox jumped     |              # A bit bad
The quick brown fox jumped over|              # Perfect!
The quick brown fox jumped over|the           # Pretty good
The quick brown fox jumped over|the lazy      # Not so good
The quick brown fox jumped over|the lazy dog. # Bad again
```

Adding more text to that row is not going to make badness *decrease* after
that, is it? It's only going to get worse.

When trying to figure out a place to break a line, a *local minimum of
badness* is what we want. Just calculate badness for a number of potential
breaking places, and choose its minimum.

Let's consider an example we found in the previous lesson and see how its
badness evolves over time. For this I wrote a tiny exploratory program
(remember those from [part 1?](/part1/recap.run.html))

```python
import boxes
text_boxes = boxes.create_text_boxes('line.txt')
for i in range(1, len(text_boxes)):
    text_boxes[i].x = text_boxes[i-1].x + text_boxes[i-1].w
for i in range(50, len(text_boxes)):
    row = text_boxes[:i+1]
    print("{:.03f} | {}".format(
        boxes.badness(25, row),
        ''.join(x.letter for x in row)))
```

Some lines seem repeated because non-visible characters are added.

```xml
2.819 | take de­light in vex­ing me. You have no com­pas­si
2.567 | take de­light in vex­ing me. You have no com­pas­sio
2.315 | take de­light in vex­ing me. You have no com­pas­sion
1.944 | take de­light in vex­ing me. You have no com­pas­sion 
1.832 | take de­light in vex­ing me. You have no com­pas­sion f
1.608 | take de­light in vex­ing me. You have no com­pas­sion fo
1.476 | take de­light in vex­ing me. You have no com­pas­sion for
1.226 | take de­light in vex­ing me. You have no com­pas­sion for 
0.931 | take de­light in vex­ing me. You have no com­pas­sion for m
0.748 | take de­light in vex­ing me. You have no com­pas­sion for my
0.589 | take de­light in vex­ing me. You have no com­pas­sion for my 
0.406 | take de­light in vex­ing me. You have no com­pas­sion for my p
0.223 | take de­light in vex­ing me. You have no com­pas­sion for my po
0.040 | take de­light in vex­ing me. You have no com­pas­sion for my poo
0.137 | take de­light in vex­ing me. You have no com­pas­sion for my poor
0.293 | take de­light in vex­ing me. You have no com­pas­sion for my poor 
0.628 | take de­light in vex­ing me. You have no com­pas­sion for my poor n
0.963 | take de­light in vex­ing me. You have no com­pas­sion for my poor ne
1.162 | take de­light in vex­ing me. You have no com­pas­sion for my poor ner
1.466 | take de­light in vex­ing me. You have no com­pas­sion for my poor nerv
1.801 | take de­light in vex­ing me. You have no com­pas­sion for my poor nerve
2.105 | take de­light in vex­ing me. You have no com­pas­sion for my poor nerves
2.272 | take de­light in vex­ing me. You have no com­pas­sion for my poor nerves.
2.471 | take de­light in vex­ing me. You have no com­pas­sion for my poor nerves.”
2.471 | take de­light in vex­ing me. You have no com­pas­sion for my poor nerves.”
```

As you can see the minimum is reached in line 14. However, that is not a good
place to break because it would leave "poo" in the first line and a solitary 
"r" in the second one, so let's iterate and filter this by showing only good
breaking points:

```python
import boxes

text_boxes = boxes.create_text_boxes("line.txt")
for i in range(1, len(text_boxes)):
    text_boxes[i].x = text_boxes[i - 1].x + text_boxes[i - 1].w
for i in range(30, len(text_boxes)):
    row = text_boxes[:i + 1]
    if row[-1].letter in [" ", "\xad", "\n"]:
        print(
            "{:.03f} | {}".format(
                boxes.badness(25, row), "".join(x.letter for x in row)
            )
        )
```

And here is the output:

```xml
8.653 | take de­light in vex­ing me. You 
6.135 | take de­light in vex­ing me. You have 
4.732 | take de­light in vex­ing me. You have no 
3.882 | take de­light in vex­ing me. You have no com­
3.150 | take de­light in vex­ing me. You have no com­pas­
1.944 | take de­light in vex­ing me. You have no com­pas­sion 
1.226 | take de­light in vex­ing me. You have no com­pas­sion for 
0.589 | take de­light in vex­ing me. You have no com­pas­sion for my 
0.293 | take de­light in vex­ing me. You have no com­pas­sion for my poor 
2.471 | take de­light in vex­ing me. You have no com­pas­sion for my poor nerves.”
```

Looks good to me. Of course, the "badness" algorithm is just guesswork, but it
appears to have the general features of what we want, so it's good enough for
a first approach.

How could we use `badness()` to improve our line breaking algorithm, which
is implemented in `fill_row()`? First, let's look at what we are doing now:

<div class='source_title'><a href="code/lesson9.2/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&117
def fill_row(boxes, page, separation):
    """Fill a row with elements removed from boxes.

    The elements put in the row should be a good fit for laying out on
    page considering separation.
    """

    row = []
    x = page.x
    while boxes:
        b = boxes.pop(0)
        row.append(b)
        b.x = x
        if is_breaking(b, page):
            break

        x = x + b.w + separation
    return row

```


<img src="graph_10.svg" class="chart">


That is very simple. It's going to become slightly more complex. Here is a
first approach:

<div class='source_title'><a href="code/lesson10/boxes.py.html" target="_blank">boxes.py</a></div>

```python
&&&117
def fill_row(boxes, page, separation):
    """Fill a row with elements removed from boxes.

    The elements put in the row should be a good fit for laying out on
    page considering separation.
    """

    # Calculate initial breaking point
    row = []
    x = page.x
    while boxes:
        b = boxes.pop(0)
        row.append(b)
        b.x = x
        if is_breaking(b, page):
            break

        x = x + b.w + separation
    # Calculate badness for previous breaking points
    badnesses = {}
    for i in range(1, len(row)):
        _row = row[:i + 1]
        if _row[-1].letter in [" ", "\xad", "\n"]:
            how_bad = badness(page.w, _row)
            badnesses[how_bad] = _row
    if badnesses:
        # Find minimum badness
        min_bad = min(badnesses.keys())
        _row = badnesses[min_bad]
        # Put leftover letters back in boxes
        for b in row[:len(_row) - 1:-1]:
            boxes.insert(0, b)
    else:
        _row = row
    return _row

```

Does it work? Yes, quite nicely! Compare: before...

![lesson9.3.svg](lesson9.3.svg)

And after...

![lesson10.svg](lesson10.svg)

Faced with an overfull line, our algorithm backtracks and breaks that line
earlier, while underfull, instead of smushing it.

However I am very, very unhappy with that code.

* All the `_row` / `row` stuff is confusing.
* The `boxes.insert()` loop is icky but necessary because otherwise those
  boxes are already popped and lost.
* We have a magic literal! `[" ", "\xad", "\n"]`

So, time for another round of refactoring, redefining our internal interfaces
and trying to make pieces fit more smoothly.

But before ... Our tests are passing! That's bad!

Why? Because it means our tests don't care *where* we break the line, or at
least they are not good enough to tell apart the two cases we just saw.

So, they need improving. Here is the new test:

<div class='source_title'><a href="code/lesson10/tests/test_fill_row.py.html" target="_blank">test_fill_row.py</a></div>

```python
&&&34
def test_fill_row_backtrack():
    letters = [boxes.Box(letter="x", x=i, w=1) for i in range(100)]
    for x in (5, 25, 40):
        letters[x].letter = " "
        letters[x].stretchy = True
    page = boxes.Box(x=0, y=0, w=30, h=30)
    row = boxes.fill_row(letters, page, 0)

    # Because of backtracking, it breaks underfull
    assert len(row) == 26
    assert len(letters) == 74
    assert row[-1].letter == " "

```

And remember: **if you change things and the tests don't break, either you are
missing tests, or the change is not meaningful.**

Finally: a quick visual check that we are not breaking something else:

```plaintext
python code/lesson10/boxes.py ./pride-and-prejudice.txt lesson10.1.svg
```

![lesson10.1.svg](lesson10.1.svg)

With a clearer conscience, we can move onto fixing that ugly function.

------

* Full source code for this lesson <a href="code/lesson10/boxes.py.html"
  target="_blank">boxes.py</a>
* <a href="code/diffs/lesson10_diff.html" target="_blank">Difference with code
  from last lesson</a>
