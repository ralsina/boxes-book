all:
	cd src; make
	./node_modules/.bin/gitbook build . public

serve:
	cd src; make
	./node_modules/.bin/gitbook serve . public

pdf:
	cd src; make
	./node_modules/.bin/gitbook pdf . boxes.pdf

epub:
	cd src; make
	./node_modules/.bin/gitbook epub . boxes.epub

.PHONY: all