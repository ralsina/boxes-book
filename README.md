# Boxes-Book

This is the source code for my book available at https://ralsina.gitlab.io/boxes-book/

The build process is pretty manual because I did not expect anyone else to use it but here it is:

* Clone this repo [git clone https://gitlab.com/ralsina/boxes-book.git --recurse-submodules]
* Get `gitbook` [npm install gitbook-cli] (pretty recent version)
* Run `gitbook install` (to get all gitbook plugins)
* Get graphviz 2.40.1 (yes, that is newer than the one you have)
* Get chroma from https://github.com/alecthomas/chroma [A x86 binary is included]
* Setup a python >= 3.6 virtualenv
* pip install -r requirements.txt
* make / make serve / make pdf

